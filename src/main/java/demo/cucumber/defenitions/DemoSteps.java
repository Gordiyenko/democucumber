package demo.cucumber.defenitions;

import demo.cucumber.page.DemoPage;
import io.cucumber.java.en.When;

public class DemoSteps {

    DemoPage mainPage = new DemoPage();

    @When("User opens main page")
    public void user_opens_main_page(){
        mainPage.init();
    }

}
